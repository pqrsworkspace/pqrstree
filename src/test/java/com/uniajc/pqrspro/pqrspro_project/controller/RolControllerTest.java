package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.modelo.dto.RolDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class RolControllerTest {

    @Autowired
    RolController rolController;

    @DisplayName(value = "test Auth -> Cuando se crea un rol de forma correcta.")
    @Test
    void testForm() {

        RolDto rolDto = new RolDto();

        rolDto.setIdrol(3L);
        rolDto.setNombrerol("Administrador");

        ResponseEntity respuestaOptenida = rolController.registrar(rolDto);

        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Auth -> Cuando se crea un rol de forma correcta.")
    @Test
    void testUpdateForm() {

        RolDto rolDto = new RolDto();

        rolDto.setIdrol(3L);
        rolDto.setNombrerol("Administrador");

        ResponseEntity respuestaOptenida = rolController.actualizar(rolDto);

        assertThat(respuestaOptenida).isNotNull();
    }

    @DisplayName(value = "test Rol -> la respuesta de la consulta trae el listado de roles")
    @Test
    void testConsutarRoles() {
        ResponseEntity resultReal = rolController.consultarRoles();
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = "test Rol -> consultar roles por id")
    @Test
    void testConsutarRolesId() {
        ResponseEntity resultReal = rolController.consultarRolById(3L);
        assertThat(resultReal).isNotNull();
    }

    @DisplayName(value = " test Auth -> cuando se elimina un id")
    @Test
    void testDeleteId() {
        ResponseEntity respuestaOptenida = rolController.eliminar(3L);
        assertThat(respuestaOptenida).isNotNull();
    }
}
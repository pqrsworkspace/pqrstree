package com.uniajc.pqrspro.pqrspro_project.repository;

import com.uniajc.pqrspro.pqrspro_project.modelo.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository <Usuario , Long> {

    Optional<Usuario> findByUsernameAndPassword(String user, String password);

    Optional<Usuario> findByUsernameAndCorreo(String user, String correo);
}

package com.uniajc.pqrspro.pqrspro_project;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
public class PqrsproProjectApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(PqrsproProjectApplication.class, args);

	}
}
package com.uniajc.pqrspro.pqrspro_project.modelo.dto;

import lombok.*;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PqrsDto implements Serializable {

    private Long id;
    private Long idestudiante;
    private String nombreestudiante;
    private String area;
    private String descripcion;
    private String estado;
    private String correo;

    public boolean validarDto() {
        if (null == id) {
            return false;
        }
        return validarCampos();
    }

    private boolean validarCampos() {

        if (StringUtils.isBlank(nombreestudiante)) {
            return false;
        }
        if (StringUtils.isBlank(area)) {
            return false;
        }
        if (StringUtils.isBlank(correo)) {
            return false;
        }
        if (StringUtils.isBlank(descripcion)) {
            return false;
        }
        if (StringUtils.isBlank(estado)) {
            return false;
        }
        if (null == idestudiante) {
            return false;
        }

        return validarCaracteres();
    }

    private boolean validarCaracteres() {

        String caracteresEspeciales = "|@&$</>";

        if (StringUtils.containsAny(nombreestudiante, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(area, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(descripcion, caracteresEspeciales)) {
            return false;
        }
        if (StringUtils.containsAny(estado, caracteresEspeciales)) {
            return false;
        }
        return true;
    }

}

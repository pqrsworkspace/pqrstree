package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.exception.PqrsException;
import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;
import com.uniajc.pqrspro.pqrspro_project.modelo.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.transformacion.RolTransformacion;
import com.uniajc.pqrspro.pqrspro_project.repository.RolRepository;
import com.uniajc.pqrspro.pqrspro_project.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RolServiceImpl implements RolService {

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private RolTransformacion rolTransformacion;

    @Override
    public String registrar(RolDto rolDtoSave) {

        if (rolDtoSave.validarDto()) {
            Rol dtoRolSave = rolTransformacion.rolDtoToRol(rolDtoSave);
            rolRepository.save(dtoRolSave);
            return "Registro Exitoso";
        }
        throw new PqrsException("Los campos no cumplen las condiciones");

    }

    @Override
    public String actualizar(RolDto rolDtoUpdate) {

        if (rolDtoUpdate.validarDto()) {
            Rol dtoRolUpdate = rolTransformacion.rolDtoToRol(rolDtoUpdate);
            rolRepository.save(dtoRolUpdate);
            return "Registro Exitoso";
        }
        throw new PqrsException("Los campos no cumplen las condiciones");
    }

    @Override
    public String eliminar(Long id) {
        if (rolRepository.existsById(id)) {
            rolRepository.deleteById(id);
            return "Eliminado con exito";
        }
        throw new PqrsException("Ocurrio un error al eliminar, el id no existe en la base de datos");

    }

    @Override
    public Rol consultarRolById(Long id) {
        return rolRepository.findById(id).orElse(null);
    }

    @Override
    public List<Rol> consultarRoles() {
        return (List<Rol>) rolRepository.findAll();
    }
}

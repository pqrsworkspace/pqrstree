package com.uniajc.pqrspro.pqrspro_project.exception;

public class PqrsException extends RuntimeException {

    public PqrsException(String mensaje){
        super(mensaje);
    }
}
